import time
from datetime import timedelta

from timer_dict import TimerDict


def test_put():
    dct = TimerDict(timedelta(seconds=1))
    dct.put("key1", "value1")
    assert "key1" in dct
    assert dct["key1"] == "value1"


def test_put_with_duration():
    dct = TimerDict(timedelta(seconds=0.1))
    dct.put("key1", "value1", duration=timedelta(seconds=0.2))
    assert "key1" in dct
    assert dct["key1"] == "value1"

    time.sleep(0.1)

    assert "key1" in dct
    assert dct["key1"] == "value1"

    time.sleep(0.2)

    assert "key1" not in dct


def test_setitem():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    assert "key1" in dct
    assert dct["key1"] == "value1"

    time.sleep(0.2)

    assert "key1" not in dct


def test_getitem():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    assert dct["key1"] == "value1"


def test_delitem():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    assert "key1" in dct

    del dct["key1"]
    assert "key1" not in dct


def test_iter():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    dct["key1"] = "value1"
    assert set(dct) == {"key1", "key1"}


def test_keys():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    dct["key1"] = "value1"
    assert set(dct.keys()) == {"key1", "key1"}


def test_values():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    dct["key1"] = "value1"
    assert set(dct.values()) == {"value1", "value1"}


def test_items():
    dct = TimerDict(timedelta(seconds=0.1))
    dct["key1"] = "value1"
    dct["key1"] = "value1"
    assert set(dct.items()) == {("key1", "value1"), ("key1", "value1")}


def test_len():
    dct = TimerDict(timedelta(seconds=0.1))
    assert len(dct) == 0
    dct["key1"] = "value1"
    assert len(dct) == 1
